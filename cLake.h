
class cLake
{
public:
    cLake()
        : myWind( 0 )
        , myXWWMark( 100 )
        , myYWWMark( 5 )
    {

    }
    /** Add boat to lake */
    void Add( cSailBoat* b );

    /** Add skipper to last boat */
    void Add( cSkipper * s )
    {
        myFleet.back().Add( s );
    }

    void Move();

    /** True if boat is above layline on its present tack
        @param[in] boat
        @return true if over layline
    */
    bool IsOverLayLine( const cSailBoat& boat ) const;

    bool IsAboveMark( const cSailBoat& boat ) const;

    /** Wind at a location
        @param[out] h heading from which wind is coming in degrees
        @param[out] v velocity
        @param[in] x location
        @param[in] y location
    */
    void Wind( float& h, float& v,
               float x, float y ) const
    {
        h = 0;
        v = 1;
    }

    float Mark( float x, float y ) const;

private:
    float myWind;
    float myXWWMark;
    float myYWWMark;
    std::vector< cSailBoat > myFleet;
};

