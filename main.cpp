#include <iostream>
#include <iomanip>
#include <sstream>
#include <vector>
#include <cmath>

#include "cSailboat.h"
#include "cLake.h"

#define DEG2RAD 0.0174533

using namespace std;


cSailBoat::cSailBoat( const std::string& name, float x, float y, float v )
    : myName( name )
    , myXLoc( x )
    , myYLoc( y )
    , myVel( v )
    , myTack( eTack::starboard )
    , myTackAngle( 40 )
    , myStatus( eStatus::upwind1 )
{

}

void cSailBoat::Location() const
{
    std::cout << myName << ", "
              << std::setprecision(3)<< myXLoc << ", " << myYLoc
              <<", "<< (int)myStatus<< "\n";
}

void cSailBoat::Trim( float wind )
{
    if( myStatus == eStatus::upwind1 )
    {
        if( myTack == eTack::starboard )
        {
            myHead = wind - myTackAngle;
        }
        else
        {
            myHead = wind + myTackAngle;
        }
    }
    else
    {
        myHead = wind + 180;
    }
    if( myHead < 0 )
        myHead += 360;
    else if( myHead > 359 )
        myHead -= 360;

//    std::cout <<"Head "<< myHead << "\n";
}

void cSailBoat::Tack( eTack t )
{
    // return immediatly if there is no change
    if( t == myTack )
        return;
    std::cout << "tacked to " << (int)t << "\n";
    myTack = t;
    myVel  = 0;
}

void cSailBoat::Move()
{
    if( myVel < myHullSpeed )
        myVel += 0.5;
    float rh = myHead * DEG2RAD;
    myXLoc += myVel * sin( rh );
    myYLoc -= myVel * cos( rh );
}


class cSkipper
{
public:

    virtual void Decision(
        const cLake& theLake,
        cSailBoat& theBoat )
    {
        // the base skipper
        // never does anything
        return;
    }
};

class BangTheCorner : public cSkipper
{
public:
    virtual void Decision(
        const cLake& theLake,
        cSailBoat& theBoat )
    {
        // decision depends on stage of the race
        switch( theBoat.Status() )
        {
        case cSailBoat::eStatus::upwind1:
            if( theLake.IsAboveMark( theBoat ) )
            {
                // turn downwind
                theBoat.Status( cSailBoat::eStatus::downwind1 );
            }
            else if( theLake.IsOverLayLine( theBoat ) )
            {
                // tack
                theBoat.Tack( cSailBoat::eTack::port );
            }
            break;
        }
    }
};

void cLake::Add( cSailBoat* b )
{
    myFleet.push_back( *b );
}

void cLake::Move()
{
    for( auto& b : myFleet )
    {
        b.Skipper()->Decision( *this, b );
        b.Trim( myWind );
        b.Move();
        b.Location();
    }
}

float cLake::Mark( float x, float y ) const
{
    float dy = y - myYWWMark;
    if( fabs( dy ) < 0.01 )
    {
        if( x < myXWWMark )
            return 90;
        else
            return -90;
    }
    return 57.2958 * atan( (x - myXWWMark ) / dy );
}

bool cLake::IsOverLayLine( const cSailBoat& boat ) const
{
    float wh, wv;
    Wind( wh, wv, boat.X(), boat.Y() );
    int mh = Mark( boat.X(), boat.Y() );
    //std::cout << "mark heading " << mh << "\n";
    if(  mh > 40  )
        return true;
    if( mh < -40 )
        return true;
    return false;
}

bool cLake::IsAboveMark( const cSailBoat& boat ) const
{
    if( boat.Y() < myYWWMark )
        return true;
    return false;
}



void UnitTest()
{
    cLake L;
    float h = L.Mark( 110, 15 );
    float h2 = L.Mark( 120, 15 );
    float h3 = L.Mark( 100, 100 );
    float h4 = L.Mark( 96, 91 );

    cSailBoat B("B", 110, 15, 1 );
    bool o = L.IsOverLayLine( B );
}

int main()
{
    UnitTest();

    cLake theLake;
    theLake.Add( new cSailBoat( "XH", 100, 100, 1 ) );
    theLake.Add( new BangTheCorner );
    for( int t = 0; t < 200; t++ )
        theLake.Move();

    return 0;
}
