class cSkipper;

class cSailBoat
{
public:
    enum class eTack
    {
        port,
        starboard
    };
    enum class eStatus
    {
        upwind1,
        downwind1,
    };
    /** Construct new sailboat
        @param[in] name
        @param[in] x location
        @param[in] y location
        @param[in] v velocity
    */
    cSailBoat( const std::string& name, float x, float y, float v );

    void Add( cSkipper* s )
    {
        mySkipper = s;
    }

    /** Change tack */
    void Tack( eTack t );

    eTack Tack()
    {
        return myTack;
    }

    /** Move the boat */
    void Move();

    /** Trim the boat
        @param[in] wind direction
    */
    void Trim( float wind );

    /** Write status to console */
    void Location() const;

    float X() const
    {
        return myXLoc;
    }

    float Y() const
    {
        return myYLoc;
    }

    cSkipper * Skipper()
    {
        return mySkipper;
    }

    eStatus Status()
    {
        return myStatus;
    }
    void Status( eStatus s )
    {
        myStatus = s;
    }
private:
    std::string myName;
    float myXLoc;
    float myYLoc;
    float myHead;               ///< Heading in degrees
    float myVel;                ///< current velocity
    float myHullSpeed;         ///< maximum velocity
    eTack myTack;               ///< current tack angle
    eStatus myStatus;           ///< race status
    float myTackAngle;          ///< smallest angle to wind capable
    cSkipper * mySkipper;
};

